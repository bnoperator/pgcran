# git

https://bitbucket.org/bnoperator/pgcran

```
git add -A && git commit -m "k8s" && git push
git tag -a 1.3.1 -m "++" && git push --tags
```

# Docker 

```
VERSION=1.3.6

git clone https://bitbucket.org/bnoperator/pgcran.git
cd pgcran
git checkout $VERSION
 
DOCKER_IMAGE_REPOSITORY="docker.pamgene.com"
IMAGE=amaurel/pgcran

docker build -t ${IMAGE}:${VERSION} .
docker tag ${IMAGE}:${VERSION} ${DOCKER_IMAGE_REPOSITORY}/${IMAGE}:${VERSION}
docker push ${DOCKER_IMAGE_REPOSITORY}/${IMAGE}:${VERSION}
echo ${DOCKER_IMAGE_REPOSITORY}/${IMAGE}:${VERSION}
```
# Pamgene CRAN mirror

```
ssh pamcloud@gitlab.pamgene.com

docker run -it --restart always --name cran -p 8180:80 -v /data/CRAN/:/var/www/html/cran -v /data/PGCRAN/:/var/www/html/pgcran -d pgcran:1.3 
   
```
 

# Repos option

```
options(repos = c("https://r.pamgene.com/pgcran", "https://r.pamgene.com/cran"))

```

# Network drive

```
net use x: \\pam03.pamgene.com\pgcran /user:pgcran xxxx 

```

# Deploy git packages

```
bntools::deployGitPackage('https://bitbucket.org/bnoperator/bntools.git', '1.7')
bntools::deployGitPackage('https://bitbucket.org/bnoperator/bn_shiny.git', '2.17')
bntools::deployGitPackage('https://bitbucket.org/bnoperator/bnutil.git', '1.8')

bntools::deployGitPackage('https://bitbucket.org/bnoperator/pgmulticore.git', '3.2')
bntools::deployGitPackage('https://bitbucket.org/bnoperator/pgpamcloud.git', '1.3')
bntools::deployGitPackage('https://bitbucket.org/bnoperator/pgcheckinput.git', '3.3')
 
```
